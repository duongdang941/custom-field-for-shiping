<?php

namespace Ecentura\CustomFieldsCheckout\Block\Adminhtml\Order\View;

use Magento\Backend\Block\Template;
use Magento\Framework\Registry;

class CustomInfo extends Template
{
    protected $registry;

    public function __construct(
        Template\Context $context,
        Registry $registry,
        array $data = []
    ) {
        $this->registry = $registry;
        parent::__construct($context, $data);
    }

    public function getOrder()
    {
        return $this->registry->registry('current_order');
    }
}
